import UIKit

//// Difference between value type and reference types
/// Value types and reference types
/// In value types each instance keeps a unique reference copy of its data, usually defined as struct, enumeration or tuple
/// In reference types instances share a single copy of the data, and the type is usually defined as a class

//// Value type example
struct Struct { var data: Int = -1 }
var a = Struct()
var b = a                        // a is copied to b
a.data = 42                        // Changes a, not b
print("\(a.data), \(b.data)")    // prints "42, -1"

//// Reference type example
class Class { var data: Int = -1 }
var x = Class()
var y = x                        // x is copied to y
x.data = 42                        // changes the instance referred to by x (and y)
print("\(x.data), \(y.data)")    // prints "42, 42"

///
//// Copy on write mechanism swift
///
/// Reference: https:///medium.com/@lucianoalmeida1/understanding-swift-copy-on-write-mechanisms-52ac31d68f2f
func print(address o: UnsafeRawPointer ) {
    print(String(format: "%p", Int(bitPattern: o)))
}

var array1: [Int] = [0, 1, 2, 3]
var array2 = array1

// Print with just assign
print(address: array1) //0x600000078de0
print(address: array2) //0x600000078de0

// Let's mutate array2 to see what's
array2.append(4)

print(address: array2) //0x6000000aa100


// Output
// 0x600000078de0 array1 address
// 0x600000078de0 array2 address before mutation
// 0x6000000aa100 array2 address after mutation

///
//// Two pointer approach / This also can be called binary search
///
/// Two pointer algorithm is a technique used to traverse through an array or a list
/// where you use two pointers that move towards each other.
/// This approach is efficient in solving problems where you need to find pairs,
/// determine the sum of elements, or find a specific target value in a sorted array.
/// Reference: https:///medium.com/@mhcsunny/two-pointer-technique-in-swift-8f4bad8cbcbe
///
/// Here’s an example of a two pointer algorithm implemented in Swift to find a pair of elements that sum to a target value in a sorted array:

func twoSum(numbers: [Int], target: Int) -> [Int] {
    var left = 0
    var right = numbers.count - 1
    while left < right {
        let sum = numbers[left] + numbers[right]
        if sum == target {
            return [left + 1, right + 1]
        } else if sum < target {
            left += 1
        } else {
            right -= 1
        }
    }
    return []
}


twoSum(numbers: [1,5,8,10,25], target: 35)


///
//// Memory management swift
/// Reference: https:///heartbeat.comet.ml/memory-management-in-swift-heaps-stacks-baa755abe16a
///

/// How is Memory Managed in Swift?
/// Swift uses ARC (Automatic Reference Counting).
/// Reference counting is the process of storing reference counts of initialized objects (normally instances of a class), pointers, or blocks. These are stored in memory and cleaned up—in other words, deallocated—once the objects are no longer needed.
/// If the memory count of an instance never goes down to 0, this causes something that’s called a retain cycle, which leads to a memory leak.

/// weak and unowned

/// Both weak and unowned references do not create a strong hold on objects.

/// weak — An object that can become nil at any point in time. These types must be optional. And the type must be declared as a variable (not a constant) since the value may be changed (to nil).

/// unowned — Assumes that the object will never become nil and is defined using non-optional types.


///
//// Concurrency
/// Reference: https:///www.kodeco.com/28540615-grand-central-dispatch-tutorial-for-swift-5-part-1-2?page=1#toc-anchor-001
///
/// Grand Central Dispatch (GCD) is a low-level API for managing concurrent operations. It can help improve your app’s responsiveness by deferring computationally expensive tasks to the background. It’s an easier concurrency model to work with than locks and threads.
/// GCD operates on dispatch queues through a class aptly named DispatchQueue. You submit units of work to this queue, and GCD executes them in a FIFO order (first in, first out), guaranteeing that the first task submitted is the first one started.
/// Dispatch queues are thread-safe, meaning you can simultaneously access them from multiple threads. GCD’s benefits are apparent when you understand how dispatch queues provide thread safety to parts of your code. The key to this is to choose the right kind of dispatch queue and the right dispatching function to submit your work to the queue.

/// Queues can be either serial or concurrent. 

/// Serial queues guarantee that only one task runs at any given time. GCD controls the execution timing. You won’t know the amount of time between one task ending and the next one beginning

/// Concurrent queues allow multiple tasks to run at the same time. The queue guarantees tasks start in the order you add them. Tasks can finish in any order, and you have no knowledge of the time it will take for the next task to start, nor the number of tasks running at any given time.

/// This is by design: Your code shouldn’t rely on these implementation details.

/// GCD provides three main types of queues:

/// Main queue: Runs on the main thread and is a serial queue.
/// Global queues: Concurrent queues shared by the whole system. Four such queues exist, each with different priorities: high, default, low and background. The background priority queue has the lowest priority and is throttled in any I/O activity to minimize negative system impact.
/// Custom queues: Queues you create that can be serial or concurrent. Requests in these queues end up in one of the global queues.
/// When sending tasks to the global concurrent queues, you don’t specify the priority directly. Instead, you specify a quality of service (QoS) class property. This indicates the task’s importance and guides GCD in determining the priority to give to the task.

/// The QoS classes are:

/// User-interactive: This represents tasks that must complete immediately to provide a nice user experience. Use it for UI updates, event handling and small workloads that require low latency. The total amount of work done in this class during the execution of your app should be small. This should run on the main thread.
/// User-initiated: The user initiates these asynchronous tasks from the UI. Use them when the user is waiting for immediate results and for tasks required to continue user interaction. They execute in the high-priority global queue.
/// Utility: This represents long-running tasks, typically with a user-visible progress indicator. Use it for computations, I/O, networking, continuous data feeds and similar tasks. This class is designed to be energy efficient. This gets mapped into the low-priority global queue.
/// Background: This represents tasks the user isn’t directly aware of. Use it for prefetching, maintenance and other tasks that don’t require user interaction and aren’t time-sensitive. This gets mapped into the background priority global queue.

/// Here’s a quick guide on how and when to use the various queues with async:

/// Main queue: This is a common choice to update the UI after completing work in a task on a concurrent queue. To do this, you code one closure inside another. Targeting the main queue and calling async guarantees that this new task will execute sometime after the current method finishes.
/// Global queue: This is a common choice to perform non-UI work in the background.
/// Custom serial queue: A good choice when you want to perform background work serially and track it. This eliminates resource contention and race conditions since only one task executes at a time. Note that if you need the data from a method, you must declare another closure to retrieve it or consider using sync.


///
///
//// Dependency Injection
///
///
/// Dependency Injection (DI) is a design pattern commonly used in software development, including iOS development with Swift. It's a technique where the dependencies of a class or module are injected from the outside rather than created within the class itself. This helps in making the code more modular, testable, and easier to maintain.
/// In iOS development with Swift, there are mainly three types of dependency injection: constructor injection, property injection, and method injection. Let's go through each type with examples.
///
/// 1. Constructor Injection: Constructor injection involves passing dependencies through a class's initializer.

// NetworkService.swift
class NetworkService {
    func fetchData() -> String {
        return "Data from network"
    }
}

// UserService.swift
class UserService {
    private let networkService: NetworkService

    // Constructor injection
    init(networkService: NetworkService) {
        self.networkService = networkService
    }

    func getUserData() -> String {
        let data = networkService.fetchData()
        return "User data: \(data)"
    }
}

// ViewController.swift
class ViewController: UIViewController {
    let networkService = NetworkService()
    let userService: UserService

    // Dependency injection in action
    init(userService: UserService) {
        self.userService = userService
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Now, you can use userService without worrying about creating its dependencies.
        let userData = userService.getUserData()
        print(userData)
    }
}

/// In the example above:

/// NetworkService is a class responsible for fetching data from the network.
/// UserService is a class that depends on NetworkService to get user data.
/// In the ViewController, we inject an instance of UserService during initialization.
/// This approach makes it easy to replace or mock dependencies during testing, as you can create mock implementations of NetworkService and inject them into UserService. It also promotes better separation of concerns and makes your code more flexible and maintainable.


/// 2. Property Injection: Property injection involves setting dependencies through properties after the object has been created. This can be useful when you need to inject dependencies after the object has been initialized.

// Property injection example
class ViewControllerTwo: UIViewController {
    var userService: UserService?

    func fetchData() {
        // Check if userService is not nil before using it
        let userData = userService?.getUserData() ?? "No user data"
        print(userData)
    }
}

/// In this example, the ViewControllerTwo has a property userService, and you can set an instance of UserService from outside the class.

/// 3. Method Injection: Method injection involves passing dependencies directly to methods that need them. This can be useful when only specific methods of a class require certain dependencies.

struct Order { }
class PaymentService {
    func processPayment(_ order: Order) { }
}

// Method injection example
class OrderProcessor {
    func processOrder(order: Order, paymentService: PaymentService) {
        // Process the order using the provided payment service
        paymentService.processPayment(order)
        // Additional order processing logic
    }
}

/// In this example, the processOrder method of OrderProcessor requires a PaymentService to process the payment. Instead of having PaymentService as a property or being injected through the constructor, it is passed directly to the method.
/// Choosing the appropriate type of dependency injection depends on the specific needs of your application and the relationships between your classes. Constructor injection is generally a good default choice, but property injection and method injection can be useful in certain situations where you need more flexibility.


//// SOLID principles resources
///
/// https://gist.github.com/mjhassan/2a61c16c68d66fd43f24306a07900bc6
/// https://medium.com/@nishant.kumbhare4/solid-principles-in-swift-73b505d3c63f
///

//// Design patterns
/// https://refactoring.guru/design-patterns/swift
/// let´s check adapter, facade and delegation pattern for payments team but also let´s take a look for other patterns in the link provided above

/// Facade
/// Facade is a structural design pattern that provides a simplified interface to a library, a framework, or any other complex set of classes.

/// Adapter
/// Adapter is a structural design pattern that allows objects with incompatible interfaces to collaborate.

///  Delegate
/// The Delegate Pattern is a behavioral design pattern where an object delegates some of its responsibilities to another object. It is commonly used in Swift, especially in iOS development, for communication between objects.

/// Singleton
/// is a creational design pattern that lets you ensure that a class has only one instance, while providing a global access point to this instance.


//// setNeedsLayout vs. layoutIfNeeded vs layoutSubviews
///
///https://abhimuralidharan.medium.com/ios-swift-setneedslayout-vs-layoutifneeded-vs-layoutsubviews-5a2b486da31c
///

//// Linked list
///
/// https://www.kodeco.com/books/data-structures-algorithms-in-swift/v3.0/chapters/6-linked-list
